#include "dynamics.hpp"

#include <iostream>
#include <iomanip>

void Dynamic_System_t::step(double step_size, CVec u)
{
  x_ = rk45(step_size, u);
  t_ = t_ + step_size;
}

void Dynamic_System_t::reset(double t0, CVec x0)
{
  t_ = t0;
  x_ = x0;
}

// rk45 implementation based on 
// http://lasp.colorado.edu/cism/CISM_DX/code/CISM_DX-0.50/required_packages/octave-forge/extra/ode/ode45.m

CVec Dynamic_System_t::rk45(double step_size, const Eigen::Ref<const CVec>& u)
{
  const double tol = .000001;
  const double hmax =  step_size / 2.5;
  const double powr = 1.0/6.0;

  static const Eigen::Matrix<double,7,6> a = (Eigen::Matrix<double,7,6>() <<
    0,               0,              0,               0,            0,              0,
    1.0/5.0,         0,              0,               0,            0,              0,
    3.0/40.0,        9.0/40.0,       0,               0,            0,              0,
    44.0/45.0,      -56.0/15.0,      32.0/9.0,        0,            0,              0,
    19372.0/6561.0, -25360.0/2187.0, 64448.0/6561.0, -212.0/729.0,  0,              0,
    9017.0/3168.0,  -355.0/33.0,     46732.0/5247.0,  49.0/176.0,  -5103.0/18656.0, 0,
    35.0/384.0,      0.0,            500.0/1113.0,    125.0/192.0, -2187.0/6784.0,  11.0/84.0).finished();

  static const Eigen::Matrix<double,7,1> b4 = (Eigen::Matrix<double,7,1>() <<
    5179.0/57600.0, 
    0.0, 
    7571.0/16695.0,
    393.0/640.0,
    -92097.0/339200.0,
    187.0/2100.0,
    1.0/40.0).finished();

  static const Eigen::Matrix<double,7,1> b5  = (Eigen::Matrix<double,7,1>() <<
    35.0/384.0,
    0.0,
    500.0/1113.0,
    125.0/192.0,
    -2187.0/6784.0,
    11.0/84.0,
    0.0).finished();

  static const Eigen::Matrix<double, 7, 1> c = (Eigen::Matrix<double,7,1>() <<
    0, 
    1.0/5.0, 
    3.0/10.0, 
    4.0/5.0, 
    8.0/9.0, 
    1.0, 
    1.0).finished();

  
  //
  double t0 = t_;
  double tfinal = t_ + step_size;
  size_t N_est_acc_steps = 5; //UGLY HACK//(tfinal - t0) * 1000; // number of estimated accepted steps
  double t_i= t0;
  double hmin = (tfinal - t_i)/1e20;
  double h = (tfinal - t_i)/2.5; // initial step size guess
  //double h = .025;
  Eigen::VectorXd x_i(x_);


  size_t num_states = x_i.rows();
  Eigen::VectorXd tout = Eigen::VectorXd::Constant(N_est_acc_steps, 0);
  Eigen::MatrixXd xout = Eigen::MatrixXd::Constant(N_est_acc_steps, num_states, 0);

  size_t Nsteps_rej = 0;
  size_t Nsteps_acc = 0;

  tout(Nsteps_acc) = t_i;
  xout.row(Nsteps_acc) = x_i.transpose();

  Eigen::MatrixXd k = Eigen::MatrixXd::Constant(num_states, 7, 0);

  k.col(0) = calc_xdot(t_i, x_i, u);

  while ( t_i< tfinal && h>=hmin)
  {
    if (t_i+ h > tfinal)
    {
      h = tfinal - t_i;
    }
    for (size_t j = 1; j < 7; ++j)
    { 
      k.col(j) = 
        calc_xdot(t_i+ c(j)*h, 
        x_i + h * k.block(0,0,num_states,j)*a.block(j,0,1,j).transpose(), u);
    }
    Eigen::VectorXd x4 = x_i + h*(k*b4);
    Eigen::VectorXd x5 = x_i + h*(k*b5);

    Eigen::VectorXd gamma1 = x5-x4;

    double delta = gamma1.lpNorm<Eigen::Infinity>();
    double tau = tol * std::max(x_i.lpNorm<Eigen::Infinity>(),1.0);

    if(delta <= tau)
    {
      t_i+= h;
      x_i = x5;
      Nsteps_acc++;
      tout(Nsteps_acc) = t_i;
      xout.row(Nsteps_acc) = x_i.transpose();
      k.col(0) = k.col(6);
    }
    else
    {
      Nsteps_rej++;
      std::cout << "rejecting step\n";
    }

    if (delta == 0.0)
    {
      delta = 1e-16;
    }
    h = std::min(hmax, .8 * h * std::pow(tau/delta,powr));
    //std::cout << "h = " << h << " accepted steps: " <<  Nsteps_acc << "\n";
    //std::cout << "tau/delta: " << tau/delta << " " << std::pow(tau/delta,powr) << " Nsteps_acc: " << Nsteps_acc << " Nsteps_rej: " << Nsteps_rej << " h: " << h << "\n";
  }

  if(Nsteps_acc < N_est_acc_steps)
  {
    tout = tout.head(Nsteps_acc+1).eval();
    xout = xout.block(0,0,Nsteps_acc+1,num_states);
  }

  if (t_i < tfinal)
  {
    std::cout << "Step size grew too small\n"; 
  }

  auto xo = xout.row(xout.rows()-1);

  return xo.transpose();
}
