#pragma once
#include <functional>
#include <eigen3/Eigen/Core>
#include <iostream>

using CVec = Eigen::VectorXd;
using Const_CVec_Ref = const Eigen::Ref<const CVec>;
using Mutable_CVec_Ref = Eigen::Ref<CVec>;

// define type for a function that takes a state vector, input vector, time and
// computes the derivate of the state vector
using ode_fun = std::function<CVec(Const_CVec_Ref x, Const_CVec_Ref u, double t)>;

class ode_stepper
{
public:
  void do_step(ode_fun sys, Mutable_CVec_Ref x, Const_CVec_Ref u,
    double t, double dt)
  {
    integrate(sys, x, u, t, dt);
  }

  virtual ~ode_stepper() = default;
private:
  virtual void integrate(ode_fun sys, Mutable_CVec_Ref x, Const_CVec_Ref u,
    double t, double dt) = 0;
};




class rk45_stepper : public ode_stepper
{
  void integrate(ode_fun sys, Mutable_CVec_Ref x, Const_CVec_Ref u,
    double t, double dt) override
  {
    static const Eigen::Matrix<double,7,6> a = (Eigen::Matrix<double,7,6>() <<
      0,               0,              0,               0,            0,              0,
      1.0/5.0,         0,              0,               0,            0,              0,
      3.0/40.0,        9.0/40.0,       0,               0,            0,              0,
      44.0/45.0,      -56.0/15.0,      32.0/9.0,        0,            0,              0,
      19372.0/6561.0, -25360.0/2187.0, 64448.0/6561.0, -212.0/729.0,  0,              0,
      9017.0/3168.0,  -355.0/33.0,     46732.0/5247.0,  49.0/176.0,  -5103.0/18656.0, 0,
      35.0/384.0,      0.0,            500.0/1113.0,    125.0/192.0, -2187.0/6784.0,  11.0/84.0).finished();

    static const Eigen::Matrix<double,7,1> b4 = (Eigen::Matrix<double,7,1>() <<
      5179.0/57600.0,
      0.0,
      7571.0/16695.0,
      393.0/640.0,
      -92097.0/339200.0,
      187.0/2100.0,
      1.0/40.0).finished();

    static const Eigen::Matrix<double,7,1> b5  = (Eigen::Matrix<double,7,1>() <<
      35.0/384.0,
      0.0,
      500.0/1113.0,
      125.0/192.0,
      -2187.0/6784.0,
      11.0/84.0,
      0.0).finished();

    static const Eigen::Matrix<double, 7, 1> c = (Eigen::Matrix<double,7,1>() <<
      0,
      1.0/5.0,
      3.0/10.0,
      4.0/5.0,
      8.0/9.0,
      1.0,
      1.0).finished();


    //
    const double tol = .000001;
    const double hmax =  dt / 2.5;
    const double powr = 1.0/6.0;
    double tfinal = t + dt;
    double hmin = (tfinal - t)/1e20;
    double h = (tfinal - t)/2.5; // initial step size guess
    Eigen::VectorXd x_i(x);

    size_t num_states = x_i.rows();

    size_t Nsteps_rej = 0;
    size_t Nsteps_acc = 0;

    Eigen::MatrixXd k = Eigen::MatrixXd::Constant(num_states, 7, 0);

    k.col(0) = sys(x_i, u, t);

    while ( t < tfinal && h>=hmin)
    {
      if (t + h > tfinal)
      {
        h = tfinal - t;
      }
      for (size_t j = 1; j < 7; ++j)
      {
        k.col(j) =
          sys(x_i + h * k.block(0,0,num_states,j)*a.block(j,0,1,j).transpose(),
          u, t + c(j)*h);
      }
      Eigen::VectorXd x4 = x_i + h*(k*b4);
      Eigen::VectorXd x5 = x_i + h*(k*b5);

      Eigen::VectorXd gamma1 = x5-x4;

      double delta = gamma1.lpNorm<Eigen::Infinity>();
      double tau = tol * std::max(x_i.lpNorm<Eigen::Infinity>(),1.0);

      if(delta <= tau) // accept the step
      {
        t += h;
        x_i = x5;
        x = x5;
        x_i = x;
        Nsteps_acc++;
        k.col(0) = k.col(6);
      }
      else
      {
        Nsteps_rej++;
        std::cout << "rejecting step\n";
      }

      if (delta == 0.0)
      {
        delta = 1e-16;
      }
      h = std::min(hmax, .8 * h * std::pow(tau/delta,powr));
    }

    if (t < tfinal)
    {
      std::cout << "Step size grew too small\n";
    }
  }
};