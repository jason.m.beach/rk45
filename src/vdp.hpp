#pragma once

#include "dynamics.hpp"


class vdp : public Dynamic_System_t
{
  double mu = 1;
  CVec calc_xdot(double, const Eigen::Ref<const CVec>&x, const Eigen::Ref<const CVec>& /*u*/)
  {
    CVec xdot;
    xdot.resize(2);
    xdot[0] = x(1);
    xdot[1] = mu * (1 - x(0)*x(0)) * x(1) - x(0);
    return xdot;
  }
public:
  vdp(double t0, CVec x0):Dynamic_System_t(t0, x0) {}
};