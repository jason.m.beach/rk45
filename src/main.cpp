#include <iostream>
#include <iomanip>
#include <chrono>

#include "ode_stepper.hpp"

struct vdp_t
{
  double mu = 1;
  CVec operator()(Const_CVec_Ref x, Const_CVec_Ref /*u*/, double /*t*/)
  {
    CVec xdot;
    xdot.resize(2);
    xdot[0] = x(1);
    xdot[1] = mu * (1 - x(0)*x(0)) * x(1) - x(0);
    return xdot;
  }
};

int main ()
{
  std::cout << std::fixed << std::setprecision(8);


  rk45_stepper stepper;
  vdp_t vdp;


  CVec x, u;
  size_t nstates = 2;
  x.resize(nstates);

  x << 2.0f, 0.0f;
  double t = 0;
  //tspan.resize(2);

  //vdp dynamic_system(0,x0);
  //MSD_State_t msd;

  double dt = .01;//.025;

//  tspan << 0.0 , h;


  for(; t < 20.0; t += dt)
  {
    std::cout << "t: " << t << " x: " << x.transpose() ;

    auto t1 = std::chrono::high_resolution_clock::now();
    stepper.do_step(vdp, x, u, t, dt);
    auto t2 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double,std::micro> fp_us = t2-t1;

    std::cout << " elapsed time: " << fp_us.count() << " us" << std::endl;
//    x0 = xout;
//    tspan(0) = tspan(1);
//    tspan(1) = tspan(0) + h;
  }
}