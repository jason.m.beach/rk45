#pragma once
#include "eigen3/Eigen/Dense"

using CVec = Eigen::VectorXd;

class Dynamic_System_t 
{
public:
  Dynamic_System_t(double t0, CVec x0):x_(x0), t_(t0) {}
  virtual ~Dynamic_System_t() = default;

  void step(double step_size, CVec u);
  
  virtual CVec calc_xdot(double, const Eigen::Ref<const CVec> &x, const Eigen::Ref<const CVec> &u) = 0;

  void reset(double t0, CVec x0);

  const CVec& x = x_; // provide read only accessors.  setters are not needed.
  const double& t = t_;

private:
  CVec x_;
  double t_;
  
  CVec rk45(double step_size, const Eigen::Ref<const CVec>& u);
};
